package com.rhf76.client.util;

public class Sleep {
    public static void go(long ms) {
        try {
            Thread.sleep(ms);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}