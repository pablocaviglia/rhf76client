package com.rhf76.client.comm;

import com.fazecast.jSerialComm.SerialPort;
import com.rhf76.client.constant.ErrorTable;
import com.rhf76.client.util.Sleep;

import java.io.BufferedOutputStream;
import java.io.IOException;

/**
 * Created by pablo on 14/01/17.
 */
public class CommFacade {

    private String portName;
    private SerialPort serialPort;

    public CommFacade(String portName) {
        this.portName = portName;
    }

    public void open() {

        //get serial port
        SerialPort[] commPorts = SerialPort.getCommPorts();
        for(SerialPort currentSerialPort : commPorts) {
            if(currentSerialPort.getSystemPortName().contains(portName)) {
                serialPort = currentSerialPort;
                break;
            }
        }
        serialPort.openPort();
    }

    public void close() {
        serialPort.closePort();
    }

    public void init() throws IOException, InterruptedException {
        writeString("AT"); //test if connection of module is OK.
        readString();
    }

    public void writeString(String str) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(serialPort.getOutputStream());
        bos.write((str + "\n").getBytes());
        bos.flush();
        bos.close();
        System.out.println("++++ SENT ++++\n" + str);
    }

    public String readString() throws IOException, InterruptedException {
        return readString(0, null);
    }

    public String readString(long timeout, String waitForEndingString) throws IOException, InterruptedException {

        while(serialPort.bytesAvailable() == 0) {
            Thread.sleep(20);
        }

        long lastAvailableByteMs = 0;
        StringBuffer stringBuffer = new StringBuffer();
        while(true) {


            if(serialPort.bytesAvailable() > 0) {
                lastAvailableByteMs = System.currentTimeMillis();
            }

            if((System.currentTimeMillis() - lastAvailableByteMs) > 50 && timeout == 0) {
                break;
            }

            byte[] readBuffer = new byte[serialPort.bytesAvailable()];
            serialPort.readBytes(readBuffer, readBuffer.length);
            stringBuffer.append(new String(readBuffer));

            if(timeout > 0) {
                Thread.sleep(250);
                timeout -= 250;

                if(waitForEndingString != null && stringBuffer.toString().trim().toLowerCase().endsWith(waitForEndingString.toLowerCase())) {
                    break;
                }
            }
        }

        String readStr = stringBuffer.toString();
        String errorStr = evaluateError(readStr);

        if(errorStr != null) {
            System.out.println("++++ RECV w/error ++++\n" + readStr + " Message=" + errorStr);
        }
        else {
            System.out.println("++++ RECV ++++\n" + readStr);
        }
        System.out.println(); //just new line

        return readStr;
    }

    public String evaluateError(String str) {

        String error = null;

        if(str != null && !str.trim().equals("")) {
            if(str.contains("ERROR(")) {

                //parse error code to int
                String numberStr = str.substring(str.indexOf('(')+1, str.indexOf(')'));
                int number = Integer.parseInt(numberStr);

                //get error
                error = ErrorTable.ERROR_CODES.get(number);
            }
        }

        return error;
    }
}