package com.rhf76.client;

import com.rhf76.client.constant.Datarate;
import com.rhf76.client.constant.DatarateScheme;
import com.rhf76.client.constant.ID;
import com.rhf76.client.logic.LogicFacade;
import com.rhf76.client.comm.CommFacade;
import com.rhf76.client.constant.Mode;
import com.rhf76.client.util.Sleep;

public class App {

    public App() throws Exception {

        LogicFacade logicFacade = new LogicFacade(new CommFacade("ttyUSB0"));
        logicFacade.init();

//        logicFacade.factoryDefault();

        /**
         * Config from
         * http://kotahi.net/risinghf/
         */

        //disable timeouts
//        logicFacade.execute("AT+UART=Timeout,0");

        // Set Device EUI
//        logicFacade.execute("at+id=DevEUI,\"0123456789ABCDEF\"");

        // Set App EUI
//        logicFacade.execute("at+id=AppEUI,\"0123456789ABCDEF\"");

        // Set Application Key.
//        logicFacade.execute("at+key=AppKey,\"00000000000000000000000000000001\"");

        logicFacade.execute("AT+DR=US915");
        logicFacade.execute("AT+DR=SCHEME");


        logicFacade.execute("AT+ADR=OFF");
        logicFacade.execute("AT+DR=DR2");
        logicFacade.execute("AT+PORT=1");
        logicFacade.execute("AT+CMSGHEX=\"31 30 30 2C 57 58\"");
        logicFacade.execute("AT+CMSGHEX=\"3130302C5758\"");

        logicFacade.execute("AT+MODE");
        logicFacade.execute("AT+CH");
        logicFacade.execute("AT+ID");
        logicFacade.execute("AT+CLASS");
        logicFacade.execute("AT+POWER");


//        logicFacade.execute("AT+DR=0");
//        logicFacade.execute("AT+DR=?");

        // Configure channel 0. Default data rates are OK.
//        logicFacade.execute("at+ch=0, 902.3");
//        logicFacade.execute("at+ch");

        // Set RX Window 2
//        logicFacade.execute("at+rxwin2=923.3, 0");
//        logicFacade.execute("at+rxwin2=923.3, 0");

        // Set transmission power to maximum
//        logicFacade.execute("at+power=20");

        // Set Adaptive Data Rate on
//        logicFacade.execute("at+adr=off");

//        logicFacade.execute("AT+RXWIN1=65,0");
//        logicFacade.execute("AT+RXWIN1=66,0");
//        logicFacade.execute("AT+RXWIN1=67,0");
//        logicFacade.execute("AT+RXWIN1=68,0");
//        logicFacade.execute("AT+RXWIN1=69,0");
//        logicFacade.execute("AT+RXWIN1=70,0");
//        logicFacade.execute("AT+RXWIN1=71,0");
//        logicFacade.execute("AT+RXWIN1=0,923.3");
//        logicFacade.execute("AT+RXWIN1=ON");

//        // Set join mode to Over The Air Activation
//        logicFacade.execute("at+mode=lwotaa");

//        logicFacade.execute("at+join", 10000, "done");

        /**
         * END MANUAL
         */



//        //change (APPEUI)
//        logicFacade.changeID(ID.APP_EUI, "0123456789ABCDEF");
//
//        //change (DEVEUI)
//        logicFacade.changeID(ID.DEV_EUI, "0123456789ABCDEF");
//
//        //change (DEVADDR)
//        logicFacade.changeID(ID.DEV_ADDR, "07ac2a84");
//
//        //change (APPKEY)
//        logicFacade.changeAppKey("00000000000000000000000000000001");
//
//        //re-read id
//        logicFacade.id();

//        logicFacade.setAdr(true);


        //set scheme
//        logicFacade.setDatarateScheme(DatarateScheme.US915);
//        logicFacade.getDatarateScheme();


//        //set data rate
//        logicFacade.setDatarate("DR0");
//        logicFacade.getDatarate();


//        //configure channels
//        logicFacade.disableChannels();
//        logicFacade.setChannel(0, 902.3f, Datarate.DR0);
//        logicFacade.setChannel(1, 902.4f, Datarate.DR0);
//        logicFacade.setChannel(2, 902.5f, Datarate.DR0);
//        logicFacade.setChannel(3, 902.6f, Datarate.DR0);
//        logicFacade.setChannel(4, 902.7f, Datarate.DR0);
//        logicFacade.getChannels();


//        for(int i=64; i<72; i++) {
//            logicFacade.setRXWIN1(i, 923.3f);
//        }
//        logicFacade.setRXWIN1(5, 923.3f);
//        logicFacade.enableRXWIN1();
//        logicFacade.disableRXWIN1();


        //set rxwin2
//        logicFacade.setRXWIN2(923.3f, Datarate.DR8);
//        logicFacade.setRXWIN2(923.3f, "SF12", 500);
//        logicFacade.getRXWIN2();

        //set mode
//        logicFacade.setMode(Mode.LWABP);
//        logicFacade.setMode(Mode.LWOTAA);
//        logicFacade.join();

//        logicFacade.getRXWIN1();
//        logicFacade.getRXWIN2();

//        Sleep.go(3000);
//        logicFacade.msg("pablo");


        //send messages
//        for(int i=0; i<10; i++) {
//            logicFacade.cmsg(String.valueOf(i));
//            Sleep.go(8000);
//        }


        System.exit(0);
    }

    public static void main(String[] args) {
        try {
            new App();
        } catch (Exception e) {
            e.printStackTrace();
            ;
        }
    }
}