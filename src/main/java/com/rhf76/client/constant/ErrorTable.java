package com.rhf76.client.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pablo on 14/01/17.
 */
public class ErrorTable {

    public static Map<Integer, String> ERROR_CODES = new HashMap<>();

    static {
        ERROR_CODES.put(-1, "The number of parameters is invalid");
        ERROR_CODES.put(-2, "The content of the parameter is invalid");
        ERROR_CODES.put(-3, "API function returns error when user parameter is passed to it");
        ERROR_CODES.put(-4, "LoRaWAN modem can't save parameter to EEPROM");
        ERROR_CODES.put(-5, "The command is disabled currently");
        ERROR_CODES.put(-6, "Unknown error occurs");
        ERROR_CODES.put(-7, "There is not enough HEAP to execute user operation");
        ERROR_CODES.put(-10, "Command unknown");
        ERROR_CODES.put(-11, "Command is in wrong format");
        ERROR_CODES.put(-12, "Command is unavailable in current mode (Check with \"AT+MODE\")");
        ERROR_CODES.put(-20, "Too many parameters. LoRaWAN modem support max 15 parameters");
        ERROR_CODES.put(-21, "Length of command is too long (exceed 528 bytes)");
        ERROR_CODES.put(-22, "Receive end symbol timeout, command must end with <LF>");
        ERROR_CODES.put(-23, "Invalid character received");
        ERROR_CODES.put(-24, "Either -21, -22 or -23");
    }
}