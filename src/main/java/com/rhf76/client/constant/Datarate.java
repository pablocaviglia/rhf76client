package com.rhf76.client.constant;

/**
 * US915/AU920
 * <p>
 * LoRaWAN          Data Rate Configuration             Indicative physical bit rate [bit/s]
 * -------          -----------------------             ------------------------------------
 * DR0              LoRa SF10/125KHz                    980
 * DR1              LoRa SF9/125KHz                     1760
 * DR2              LoRa SF9/125KHz                     1760
 * DR3              LoRa SF8/125KHz                     3125
 * DR4              LoRa SF8/500KHz                     12500
 * DR5-DR7          RFU                                 RFU
 * DR8              LoRa SF12/500KHz                    980
 * DR9              LoRa SF11/500KHz                    1760
 * DR10             LoRa SF10/500KHz                    3900
 * DR11             LoRa SF9/500KHz                     7000
 * DR12             LoRa SF8/500KHz                     12500
 * DR13             LoRa SF7/500KHz                     21900
 * DR14-15          RFU                                 RFU
 */
public enum Datarate {

    DR0, DR1, DR2, DR3, DR4, DR5_DR7, DR8, DR9, DR10, DR11, DR12, DR13, DR14_15;

    public static String getRisingForceString(Datarate dr) {
        switch (dr) {
            case DR0:
                return "DR0";
            case DR1:
                return "DR1";
            case DR2:
                return "DR2";
            case DR3:
                return "DR3";
            case DR4:
                return "DR4";
            case DR5_DR7:
                return "RFU"; // TODO Is it ok this string???
            case DR8:
                return "DR8";
            case DR9:
                return "DR9";
            case DR10:
                return "DR10";
            case DR11:
                return "DR11";
            case DR12:
                return "DR12";
            case DR13:
                return "DR13";
            case DR14_15:
                return "RFU"; // TODO Is it ok this string???
        }
        return null;
    }


}
