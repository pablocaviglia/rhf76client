package com.rhf76.client.constant;

public enum Mode {

    TEST, LWOTAA, LWABP;

    public static String getRisingForceString(Mode mode) {
        switch (mode) {
            case TEST:
                return "TEST";
            case LWABP:
                return "LWABP";
            case LWOTAA:
                return "LWOTAA";
        }
        return null;
    }
}