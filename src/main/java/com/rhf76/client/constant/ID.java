package com.rhf76.client.constant;

public enum ID {

    DEV_ADDR, DEV_EUI, APP_EUI;

    public static String getRisingForceString(ID id) {
        switch (id) {
            case DEV_ADDR:
                return "DEVADDR";
            case DEV_EUI:
                return "DEVEUI";
            case APP_EUI:
                return "APPEUI";
        }
        return null;
    }
}
