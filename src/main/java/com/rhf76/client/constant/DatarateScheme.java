package com.rhf76.client.constant;

public enum DatarateScheme {

    EU868, US915, CUSTOM;

    public static String getRisingForceString(DatarateScheme id) {
        switch (id) {
            case EU868:
                return "EU868";
            case US915:
                return "US915";
            case CUSTOM:
                return "CUSTOM";
        }
        return null;
    }
}
