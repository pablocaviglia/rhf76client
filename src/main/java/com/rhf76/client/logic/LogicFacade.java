package com.rhf76.client.logic;

import com.rhf76.client.constant.DatarateScheme;
import com.rhf76.client.comm.CommFacade;
import com.rhf76.client.constant.Datarate;
import com.rhf76.client.constant.ID;
import com.rhf76.client.constant.Mode;

import java.io.IOException;
import java.text.MessageFormat;

/**
 * Created by pablo on 14/01/17.
 */
public class LogicFacade {

    private CommFacade commFacade;

    public LogicFacade(CommFacade commFacade) {
        this.commFacade = commFacade;
    }

    public void init() throws IOException, InterruptedException {
        commFacade.open();
        commFacade.init();
    }

    public void factoryDefault() throws IOException, InterruptedException {
        commFacade.writeString("AT+FDEFAULT=RISINGHF");
        commFacade.readString();
    }

    public void execute(String command) throws IOException, InterruptedException {
        commFacade.writeString(command);
        commFacade.readString();
    }

    public void execute(String command, long timeout, String waitForEndingString) throws IOException, InterruptedException {
        commFacade.writeString(command);
        commFacade.readString(timeout, waitForEndingString);
    }

    public void at() throws IOException, InterruptedException {
        commFacade.writeString("AT");
        commFacade.readString();
    }

    public void id() throws IOException, InterruptedException {
        commFacade.writeString("AT+ID");
        commFacade.readString();
    }

    public void version() throws IOException, InterruptedException {
        commFacade.writeString("AT+VER");
        commFacade.readString();
    }

    public void disableTimeout() throws IOException, InterruptedException {
        commFacade.writeString("AT+UART=TIMEOUT, 0");
        commFacade.readString();
    }

    /**
     * TODO Cant keep comm alive after executing this command
     * @throws IOException
     * @throws InterruptedException
     */
    public void reset() throws IOException, InterruptedException {
        commFacade.writeString("AT+RESET");
        commFacade.readString();
    }

    public void changeID(ID id, String newId) throws IOException, InterruptedException {
        Object[] params = new Object[]{ID.getRisingForceString(id), newId};
        String msg = MessageFormat.format("AT+ID={0}, \"{1}\"", params);
        commFacade.writeString(msg);
        commFacade.readString();
    }

    public void changeAppKey(String appKey) throws IOException, InterruptedException {
        Object[] params = new Object[]{appKey};
        String msg = MessageFormat.format("AT+KEY=APPKEY, \"{0}\"", params);
        commFacade.writeString(msg);
        commFacade.readString();
    }

    /**
     * Use to send string format frame which is no need to be confirmed by the server.
     *
     * @param msgStr
     * @throws IOException
     * @throws InterruptedException
     */
    public void msg(String msgStr) throws IOException, InterruptedException {
        Object[] params = new Object[]{msgStr};
        String msg = MessageFormat.format("AT+MSG=\"{0}\"", params);
        commFacade.writeString(msg);
        commFacade.readString();
    }

    public void testTXCLORA() throws IOException, InterruptedException {
        commFacade.writeString("AT+TEST=TXCLORA");
        commFacade.readString();
    }

    public void linkCheckReq() throws IOException, InterruptedException {
        commFacade.writeString("AT+MSG");
        commFacade.readString();
    }

    /**
     * Use LoRaWAN defined DRx to set datarate of LoRaWAN AT modem. Refer to Table 2-6 LoRaWAN
     * EU868/EU434 Data Rate Scheme and Table 2-7 LoRaWAN US915/AU920 Data Rate Scheme about
     * the detailed definition of LoRaWAN data rate.
     * @throws IOException
     * @throws InterruptedException
     */
    public void setDatarate(Datarate datarate) throws IOException, InterruptedException {
        Object[] params = new Object[]{Datarate.getRisingForceString(datarate)};
        String msg = MessageFormat.format("AT+DR={0}", params);
        commFacade.writeString(msg);
        commFacade.readString();
    }

    public void setDatarate(String datarate) throws IOException, InterruptedException {
        Object[] params = new Object[]{datarate};
        String msg = MessageFormat.format("AT+DR={0}", params);
        commFacade.writeString(msg);
        commFacade.readString();
    }

    public void getDatarate() throws IOException, InterruptedException {
        commFacade.writeString("AT+DR=?");
        commFacade.readString();
    }

    public void getDatarateScheme() throws IOException, InterruptedException {
        commFacade.writeString("AT+DR=SCHEME");
        commFacade.readString();
    }

    public void setDatarateScheme(DatarateScheme scheme) throws IOException, InterruptedException {
        Object[] params = new Object[]{DatarateScheme.getRisingForceString(scheme)};
        String msg = MessageFormat.format("AT+DR={0}", params);
        commFacade.writeString(msg);
        commFacade.readString();
    }

    public void setMode(Mode mode) throws IOException, InterruptedException {
        Object[] params = new Object[]{Mode.getRisingForceString(mode)};
        String msg = MessageFormat.format("AT+MODE={0}", params);
        commFacade.writeString(msg);
        commFacade.readString();
    }

    /**
     * Check uplink and downlink counter
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void checkUDLinkCounters() throws IOException, InterruptedException {
        commFacade.writeString("AT+LW=ULDL");
        commFacade.readString();
    }

    public void join() throws IOException, InterruptedException {
        commFacade.writeString("AT+JOIN");
        commFacade.readString();
    }

    /**
     * Set channel parameter of LoRaWAN modem, Set frequency zero to disable one channel.
     * Format:
     * AT+CH="LCn", ["Freq"], ["DR_MIN"], ["DR_MAX"]
     * // Change the LCn channel frequency to "Freq"
     * // "Freq" is in MHz. (ex. 915.5)
     * // Available DR_MIN/DR_MAX range DR0 ~ DR15
     *
     * @param channel
     * @param frequency
     * @param datarate
     * @throws IOException
     * @throws InterruptedException
     */
    public void setChannel(int channel, float frequency, Datarate datarate) throws IOException, InterruptedException {

        String dataRatesStr = "";
        if(datarate != null) {
            dataRatesStr += ", " + Datarate.getRisingForceString(datarate);
        }

        Object[] params = new Object[]{channel, frequency, dataRatesStr};
        String msg = MessageFormat.format("AT+CH={0}, {1} {2}", params);
        commFacade.writeString(msg);
        commFacade.readString();

    }

    public void setRXWIN1(int channel, float frequency) throws IOException, InterruptedException {
        Object[] params = new Object[]{channel, frequency};
        String msg = MessageFormat.format("AT+RXWIN1={0},{1} ", params);
        commFacade.writeString(msg);
        commFacade.readString();

    }

    public void getRXWIN1() throws IOException, InterruptedException {
        commFacade.writeString("AT+RXWIN1");
        commFacade.readString();
    }

    public void enableRXWIN1() throws IOException, InterruptedException {
        commFacade.writeString("AT+RXWIN1=ON");
        commFacade.readString();
    }

    public void disableRXWIN1() throws IOException, InterruptedException {
        commFacade.writeString("AT+RXWIN1=OFF");
        commFacade.readString();
    }

    /**
     * Set second RX window frequency and Data Rate. This command will change RXWIN2 configuration,
     * which may cause downlink lost, if configuration is wrong.
     *
     * @param frequency
     * @param datarate
     * @throws IOException
     * @throws InterruptedException
     */
    public void setRXWIN2(float frequency, Datarate datarate) throws IOException, InterruptedException {
        Object[] params = new Object[]{frequency, Datarate.getRisingForceString(datarate)};
        String msg = MessageFormat.format("AT+RXWIN2={0},{1} ", params);
        commFacade.writeString(msg);
        commFacade.readString();
    }

    public void setRXWIN2(float frequency, String sf, int bw) throws IOException, InterruptedException {
        Object[] params = new Object[]{frequency, sf, bw};
        String msg = MessageFormat.format("AT+RXWIN2={0},{1},{2}", params);
        commFacade.writeString(msg);
        commFacade.readString();
    }

    public void getRXWIN2() throws IOException, InterruptedException {
        commFacade.writeString("AT+RXWIN2");
        commFacade.readString();
    }

    public void setAdr(boolean enabled) throws IOException, InterruptedException {
        Object[] params = new Object[]{enabled ? "ON" : "OFF"};
        String msg = MessageFormat.format("AT+ADR={0}", params);
        commFacade.writeString(msg);
        commFacade.readString();
    }

    public void setChannel(int channel, float frequency) throws IOException, InterruptedException {
        setChannel(channel, frequency, null);
    }

    public void getChannels() throws IOException, InterruptedException {
        commFacade.writeString("AT+CH");
        commFacade.readString();
    }

    public void disableChannel(int channel) throws IOException, InterruptedException {
        Object[] params = new Object[]{channel};
        String msg = MessageFormat.format("AT+CH={0}, 0", params);
        commFacade.writeString(msg);
        commFacade.readString();
    }

    public void disableChannels() throws IOException, InterruptedException {
        for(int i=0; i<72; i++) {
            disableChannel(i);
        }
    }

    /**
     * Use to send string format frame which must be confirmed by the server.
     *
     * @param msgStr
     * @throws IOException
     * @throws InterruptedException
     */
    public void cmsg(String msgStr) throws IOException, InterruptedException {
        Object[] params = new Object[]{msgStr};
        String msg = MessageFormat.format("AT+CMSG=\"{0}\"", params);
        commFacade.writeString(msg);
        commFacade.readString();
    }
}
